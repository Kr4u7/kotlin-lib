import org.gradle.jvm.tasks.Jar

group = "app.shosetsu.lib"
version = "1.2.0"
description = "Kotlin library for shosetsu"

plugins {
	kotlin("jvm") version "1.8.0"
	id("org.jetbrains.dokka") version "1.7.20"
	kotlin("plugin.serialization") version "1.8.0"
	id("org.gradle.maven-publish")
}

val dokkaJar by tasks.creating(Jar::class) {
	group = JavaBasePlugin.DOCUMENTATION_GROUP
	description = "Assembles Kotlin docs with Dokka"
}

java {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}

publishing {
	publications {
		create<MavenPublication>("maven") {
			groupId = project.group.toString()
			artifactId = "kotlin-lib"
			version = project.version.toString()

			from(components["kotlin"])
		}
	}
}

repositories {
	mavenCentral()
	maven("https://jitpack.io")
}

dependencies {
	dokkaHtmlPlugin("org.jetbrains.dokka:kotlin-as-java-plugin:1.7.20")

	// ### Core Libraries
	api("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
	api("org.jetbrains.kotlinx:kotlinx-datetime:0.4.0") // For MPP time control

	// java only
	api("org.jsoup:jsoup:1.15.3")
	api("org.luaj:luaj-jse:3.0.1")
	api("com.squareup.okhttp3:okhttp:4.10.0")
	implementation("com.google.guava:guava:31.1-jre")


	// ### Testing Libraries
	testImplementation(kotlin("stdlib"))
	testImplementation(kotlin("stdlib-jdk8"))

	//testImplementation("net.java.dev.jna:jna:5.8.0") // for KTS

	testImplementation(kotlin("reflect"))
	//testImplementation(kotlin("script-runtime"))
	//testImplementation(kotlin("script-util"))
	//testImplementation(kotlin("compiler-embeddable"))
	//testImplementation(kotlin("scripting-compiler-embeddable"))
	//testImplementation(kotlin("script-util"))

	testImplementation(kotlin("test"))
}
